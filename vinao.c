#include <msp430.h>

/*
 * P1.0 -> saida do clock externo para o RTC - 32768Hz
 * P1.6 -> SCL
 * P1.7 -> SDA
 *
 * Endereco sensor de temperatura para I2C -> 0x27 -> ultimo bit já esta setado para read = 1
 * Endereco RTC para I2C -> 0xD1 ultimo bit está como write = 0
 *
 * MCLK = 16MHz
 * SMCLK = 4MHz
 * ACLK = 32768Hz
 *
 *
 * I2C config -> endereco de 7 bits + 0 para escrita ou 1 para leitura
 * Para receber alguma coisa, é preciso setar o N_RX_I2C_BYTES com a quantidade de bytes a ser recebida
 * 1 -> Colocar os dados no TX_I2C_DATA antes de enviar o I2C;

O programa vai funcionar da seguinte forma:

- Será enviado a data e hora atual pelo computador <10,11,15;11,37,00> através da UART
- O MSP irá gerar uma RTI com a recepção dos dados, a RTI será desabilitada e irá enviar os dados para o RTC
- O TA0 será ativado na RTI da UART e irá gerar uma RTI a cada 1 segundo
- Na RTI do TA0 será feita a leitura da temperatura e humidade do sensor HIH, a hora no sensor RTC.
- Os dados serão enviados para o computador através da UART

HUMIDADE TERÁ 3 DIGITOS 0-100

humidade em % = 100*(saida_14_bits/(2^14)-2)

TEMPERATURA TERÁ 2 DIGITOS

temperatura em celsius = (saida_14_bits/(2^14)-2)*165 - 40;


O SCL do HIH deve ser maior que 100Khz e menor que 400Khz  
o SCL do RTC deve ser maior que 0Hz e menor que 100Khz           

O HIH demora entre 6 e 8 segundos para realizar uma leitura segundo o datasheet

 */

/*
deletar depois, comentarios da migração do código do bertotti para o nosso

TX_I2C_DATA -> vet armazena os dados

*/
#define MASTER_ADDR 0x00
#define HIH_ADDR 0x27
#define RTC_ADDR 0xXX 

// Definicoes para USCI_SW_FLAGS
#define  TX_NACK  	BIT0

unsigned char TX_I2C_DATA[3], RX_I2C_DATA[10],TEMP_DATA[4]; //temp data é usado para as formulas de temperatura e humidade
unsigned char N_RX_I2C_BYTES = 0, N_TX_I2C_BYTES = 0, RX_I2C_count = 0, TX_I2C_count = 0;
unsigned char TX_UART_string[30] = "<00,00,00;00,00,00;000;00>\n@", RX_UART_string[20], HIH_vetor[4];
unsigned char tx_UART_index=0,rx_UART_index=0;
unsigned char USCI_SW_FLAGS = 0;
unsigned long int temperatura, humidade;
int tempi,flag=0; //variavel temporaria

/* FUNCOES */

void config_clock(){
	WDTCTL = WDTPW | WDTHOLD;
	// Configuracoes do BCS
    // LFXT1 desativado
    // MCLK = DCOCLK ~ 16 MHz
    // ACLK = desativado!!!
    // SMCLK = DCOCLK / 4 ~ 4 MHz
    DCOCTL = CALDCO_16MHZ;  // Usando valores de calibracao para 16MHz
    BCSCTL1 = CALBC1_16MHZ;
    BCSCTL2 = DIVS1;
    BCSCTL3 = XCAP0 + XCAP1;
    while(BCSCTL3 & LFXT1OF);
    __enable_interrupt();

}//config_clock

void config_doors(){

	P1DIR = 0xFF;
	P1OUT = BIT6 + BIT7;
  P1SEL = BIT1 + BIT2 + BIT6 + BIT7;
  P1SEL2 = BIT1 + BIT2 + BIT6 + BIT7;
	
  P2DIR = 0xFF;
  P2OUT = 0x00;

}//config_doors

void config_I2C(int fat_divisao){

/* INICIALIZACAO USCI
 *   1 - Setar bit UCSWRST (mantem USCI no estado de RESET)
 *   2 - Configurar os registrados da USCI com UCSWRST = 1
 *   3 - Configurar Porta 1 (alterar funcao dos pinos)
 *   4 - Limpar bit UCSWRST (coloca USCI em operacao)
 *   5 - Habilitar interrupcoes via UCxRXIE e/ou UCxTXIE
 */

  // Passo 1: Setar bit UCSWRST (mantem USCI no estado de RESET)
  UCB0CTL1 |= UCSWRST;

  // Passo 2: Configurar os registrados da USCI

  /* Configuracao de UCB0CTL0
   *  - Modo de endereco do MESTRE: 7 bits  (UCA10 -> 0)
   *  - Modo de endereco do ESCRAVO: 7 bits   (UCSLA10 -> 0)
   *  - Modo Multimestre: desabilitado        (UCMM -> 0)
   *  - Modo MESTRE/ESCRAVO: modo mestre    (UCMST -> 1)
   *  - Modo de operacao da USCI: I2C     (UCMODE0 -> 1; UCMODE1 -> 1)
   *  - Modo assincrono/sincrono: sincrono    (UCSYNC -> 1)
   */
  // UCB0CTL0    bit 7   bit 6    bit 5   bit 4    bit 3     bit 2    bit 1   bit 0
  //         UCA10  UCSLA10   UCMM   Resevado  UCMST   UCMODE0   UCMODE1  UCSYNC
  //         0    0    0      0      1    1     1     1
  UCB0CTL0 = UCMST + UCMODE0 + UCMODE1 + UCSYNC;

  /* Configuracao de UCB0CTL1
   *   - Fonte de clock: SMCLK         (UCSSEL0 -> 1; UCSSEL1 -> 1)
   *   - Transmissao/Recepcao: transmissao (UCTR -> 1)
   *   - Transmitir um NACK:  nao      (UCTXNACK -> 0)
   *   - Transmitir um STOP: nao       (UCTXSTP -> 0)
   *   - Transmitir um START: nao      (UCTXSTT -> 0)
   *   - Reset da USCI: manter       (UCSWRST -> 1)
   */
  // UCB0CTL1    bit 7    bit 6    bit 5    bit 4   bit 3     bit 2    bit 1    bit 0
  //        UCSSEL1  UCSSEL0  Resevado  UCTR   UCTXNACK  UCTXSTP  UCTXSTT  UCSWRST
  //         1     1       0    1   0     0     0     1
  UCB0CTL1 = UCSSEL1 + UCSSEL0 + UCTR + UCSWRST;

  /* UCB0BR0 e UCB0BR1
   *
   * Como SMCLK ~ 4MHz e o clock desejado para a I2C eh de 250 kHz, entao o fator
   * de divisao sera de 16
   *
   */
  UCB0BR0 = fat_divisao;
  UCB0BR1 = 0;

  // Passo 3: Configuracao da Porta 1
  // P1.6 como SCL e P1.7 como SDA
  P1SEL |= BIT6 + BIT7;
  P1SEL2 |= BIT6 + BIT7;

  // Passo 4: Limpar bit UCSWRST (coloca USCI em operacao)
  UCB0CTL1 &= ~UCSWRST;


  // Passo 5: Habilitar interrupcao de TX e RX
  IE2 |= UCB0TXIE + UCB0RXIE;
  /* Habilita interrucap quando um ACK NAO eh recebido do ESCRAVO
   *
   * Habilita interrucap do STOP. Quando um STOP eh gerado,  gera interrupcao
   * e na RTI reseta bit TX_OK/RX_OK de USCI_SW_FLAGS, indicando que
   * a comunicacao terminou
   */
  UCB0I2CIE = UCNACKIE + UCSTPIE;
}

void envia_i2c(volatile unsigned char SLAVE_ADDR, volatile unsigned char Num_bytes){
/*FUNCAO PARA ENVIO DE BYTES VIA I2C
 *
 * SLAVE_ADDR => Endereco do escravo
 * Comando  =>  codigo do byte de comando
 * Num_bytes => Numero de Bytes a ser transmitido
 * *P_dados  => Ponteiro para o vetor de dados
 *
 * OBS.: Antes de transmitir, os dados a serem transmitivos devem ser carregados em TX_I2C_DATA[]
 */

  /* UCB0STAT ->  UCBBUSY
   *
   * UCBBUSY eh setado apos um START e LIMPO apos um STOP
   * Entao pode ser usado para verificar se a USCI esta livre.
   *
   */

  while( UCB0STAT & UCBBUSY );

  N_TX_I2C_BYTES = Num_bytes;

  UCB0CTL1 |= UCTR;  // Seleciona transmissao
  UCB0I2CSA = SLAVE_ADDR;
  UCB0CTL1 |= UCTXSTT; // Start de transmissao

}

void recebe_i2c(volatile unsigned char SLAVE_ADDR, volatile unsigned char Num_bytes){
/*FUNCAO PARA RECEBER BYTES VIA I2C
 *
 * SLAVE_ADDR => Endereco do escravo
 * Num_bytes => Numero de Bytes a ser recebido
 * Os dados recebidos sao armazenados em RX_I2C_DATA
 */

  while( UCB0STAT & UCBBUSY ); // verificar se a USCI esta livre
  N_RX_I2C_BYTES = Num_bytes;
  UCB0CTL1 &= ~UCTR;  // Seleciona recepcao
  UCB0I2CSA = SLAVE_ADDR;
  UCB0CTL1 |= UCTXSTT; // Start de recepcao
}

//configura o TA0 parado
void config_TA0(void){
    //TA0CTL = TASSEL0;
   // TA0CCTL0 = CCIE;
    //TA0CCR0 = 32767;
    TA0CTL = TASSEL_1; // sem o MC_1 ele inicia parado
    TA0CCTL0 = CCIE;
    //TA0CCR0 = 32767;//1seg
    TA0CCR0 = 65500;//2seg
}

void config_UART(void){

    UCA0CTL1 = UCSSEL1 + UCSWRST;  // Fonte clock: SMCLK ~ 4 MHz | Interface em estado de reset
    UCA0BR0 = 0xA0;  // Para 9600 bps, conforme Tabela 15-4 do User Guide
    UCA0BR1 = 0x01;
    UCA0MCTL = UCBRS1 + UCBRS2;  // UCBRF = 0; UCBRS = 6
    UCA0CTL1 &= ~UCSWRST; // Coloca a interface no modo normal
    IFG2 &= ~UCA0TXIFG;  // Limpa flag de int. de TX, pois o bit eh setado apos reset
    IE2 |= UCA0TXIE + UCA0RXIE; // Habilitando a geracao de int. para RX e TX
}

/* TRATAMENTO DAS RTIs */

// RTI do modo MESTRE da UCA0/UCB0
//RTI de Transmissao
#pragma  vector=USCIAB0TX_VECTOR
__interrupt void USCIAB0TX_RTI(void){

/* *****************************   MODO I2C -> MESTRE   **********************************
 *
 * Quando ocorre uma TRANSMISSAO ou RECEPCAO pela USCI no modo MESTRE E
 * a interrupcao de TRANSMISSAO esta habilitada (UCB0TXIE/UCA0TXIE -> 1).
 *
 * CONSIDERANDO: UCB0TXIE/UCA0TXIE -> 1
 *  - Se for uma TRANSMISSAO, a flag UCB0TXIFG/UCA0TXIFG eh setada e cai nesta RTI
 *  - Se for uma RECEPCAO, a flag UCB0RXIFG/UCA0RXIFG eh setada e cai nesta MESMA RTI
 *
 */

//TRANSMISSAO UART
    if(IFG2 & UCA0TXIFG){
        if(TX_UART_string[tx_UART_index] == '@'){
            tx_UART_index = 0;
            IFG2 &= ~UCA0TXIFG;  // Limpa a flag de int. para nao entrar mais na RTI
        }else{
            UCA0TXBUF = TX_UART_string[tx_UART_index];
            tx_UART_index++;
        }
    }

// UCB0 MODO MESTRE -> TRANSMISSAO
  if(IFG2 & UCB0TXIFG){
    // Entra aqui apos a TRANSMISSAO do ENDERECO do ESCRAVO
    // ou quando um BYTE eh TRANSMITIDO

    IFG2 &= ~UCB0TXIFG;
    if(N_TX_I2C_BYTES <= 0){
      UCB0CTL1 |= UCTXSTP;
      TX_I2C_count = 0;
    }else{
      N_TX_I2C_BYTES--;
      UCB0TXBUF = TX_I2C_DATA[TX_I2C_count];
      TX_I2C_count++;
    }
  }

// UCB0 MODO MESTRE -> RECEPCAO
  if(IFG2 & UCB0RXIFG){
  // Entra se um Byte for recebido pela USCI no modo MESTRE

    RX_I2C_DATA[RX_I2C_count] = UCB0RXBUF;  // Armazena dado recebido no vetor
    RX_I2C_count++;

    N_RX_I2C_BYTES--;

    if(N_RX_I2C_BYTES < 1){
      RX_I2C_count = 0;
      UCB0CTL1 |= UCTXSTP;  // Se resta 1 Byte a receber apenas
    }
  }

// UCB0 MODO MESTRE -> NACK recebido
  if(UCB0STAT & UCNACKIFG){ // Se o ESCRAVO nao enviar confirmacao
      UCB0CTL1 |= UCTXSTP;      // Gera condicao STOP
      USCI_SW_FLAGS |= TX_NACK; // Indica que houve um NACK
  }
/* ************************** UCA0 ->   MODO SPI / MODO UART  **********************/

/* *********************************************************************************/
}

//RTI do TA0 - Aqui acontece toda a magia
#pragma vector=TIMER0_A0_VECTOR
__interrupt void TACCR0_RTI(void){

    //desabilita interrupção da recepcao da uart, pois não é mais necessária
    //esta sendo feita aqui pois se fizer na RTI_RXD da pau
    //IE2 &=~(UCA0RXIE);

    //led vermelho para testes e debug do funcionamento, não necessário
    P1OUT ^= BIT0;
    flag = 1;
    //inicia transmissao para o computador
    IFG2 |= UCA0TXIFG;

}

//RTI de Recepcao Serial
#pragma vector=USCIAB0RX_VECTOR     //Recebe UART
__interrupt void RTIII_RXD(void){

  //le os dados
  RX_UART_string[rx_UART_index] = UCA0RXBUF;
  rx_UART_index++;
  if(rx_UART_index >= 20) rx_UART_index = 0;

  //habilita o TA0 - ele que inicia a UART que vai pro pc
  TA0CTL |= MC0;

}
/* MAIN DO PROJETO */
int main(void) {

    WDTCTL = WDTPW + WDTHOLD; //para o watchdog timer

    //configura clock
    config_clock();
    //configura portas
    config_doors();
    //configura UART
    config_UART();
    //configura TA0 parado - Tint = 1 seg
    config_TA0();


    //executando aqui pois só tem o HIH no momento
    config_I2C(16);
    do{

      switch(flag){

        case 0:
          //aguarda alguma coisa   
        break;

        case 1:

          //teste git
        	//configura I2c para HIH
          //config_I2C(16); //4mhz/16 = 250Khz -> Dentro faixa de funcionamento do HIH
        	//faz a leitura do HIH
          TX_I2C_DATA[0] = 0x00;
          TX_I2C_DATA[1] = 0x00;
          envia_i2c(HIH_ADDR, 1);
          recebe_i2c(HIH_ADDR,4);
          TEMP_DATA[0] = RX_I2C_DATA[1];
          TEMP_DATA[1] = RX_I2C_DATA[2];
          TEMP_DATA[2] = RX_I2C_DATA[3];
          TEMP_DATA[3] = RX_I2C_DATA[4];

          //calcula
          TEMP_DATA[0] &= ~(BIT6 + BIT7);
          humidade = (TEMP_DATA[0]*256) + TEMP_DATA[1];
          TEMP_DATA[3] = TEMP_DATA[3] >> 2;
          temperatura = (TEMP_DATA[2]*64) + TEMP_DATA[3];

          temperatura = (temperatura*165/16382)-40;
          humidade = (humidade*100/16382);
          

        	//configura I2C para o RTC

        	//faz a leitura do RTC

          //esta aqui temporariamente, ativa o TA0
          //TA0CTL |= MC0;

        	//faz os calculos necessários e armazena no vetor TX_UART_STRING (Vai ser enviado na RTI do TA0 que irá ativar a UART)
        break; //fim case 1

      }
    }while(1);
}