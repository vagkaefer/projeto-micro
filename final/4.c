#include <msp430g2553.h>

#define MAX_TX_BYTES    12  // numero maximo de bytes a ser transmitido
#define MAX_RX_BYTES    12  // numero maximo de bytes a ser recebido

#define MASTER_ADDR       0x00   // Endereco do MESTRE
#define SLAVE_1_ADDR      0x27   // Endereco do ESCRAVO 1 (7 bits mais significativos)

// Definicoes para USCI_SW_FLAGS
#define  TX_NACK    BIT0

void config_ini(void);
void config_UART(void);
void P1_P2_config(void);
void UCB0_config(int divisor);
void I2C_Enviar(volatile unsigned char SLAVE_ADDR, volatile unsigned char Num_bytes);
void I2C_Receber(volatile unsigned char SLAVE_ADDR, volatile unsigned char Num_bytes);

unsigned char TX_DATA[MAX_TX_BYTES],TEMP_DATA[4];   // Vetor que armazena os dados para transmissao
unsigned char RX_DATA[MAX_RX_BYTES],RX_DATA2[MAX_RX_BYTES];   // Vetor que armazena os dados recebidos
unsigned char N_TX_BYTES = 0, N_RX_BYTES = 0, RX_count = 0, TX_count = 0;
unsigned long int temperatura, umidade;
unsigned char USCI_SW_FLAGS = 0;
int flag=0,etapa=0;
unsigned long int i;

unsigned char TX_I2C_DATA[3], RX_I2C_DATA[10],TEMP_DATA[4]; //temp data é usado para as formulas de temperatura e umidade
unsigned char N_RX_I2C_BYTES = 0, N_TX_I2C_BYTES = 0, RX_I2C_count = 0, TX_I2C_count = 0;
unsigned char TX_UART_string[30] = "<00,00,00;00,00,00;000;00>\n@", RX_UART_string[20], HIH_vetor[4];
unsigned char tx_UART_index=0,rx_UART_index=0;

void config_TA0(void){

  // taclk = ACLK = 32768 Hz, Modo UP, Interrup. a cada 1s

  TA0CTL = TASSEL0;//tirou o MC0 para iniciar parado

  TA0CCTL0 = CCIE;

  TA0CCR0 = 65530;

}

void enviarHora(void){

  /*
  TX_DATA[0] = 0;
  TX_DATA[1] = 0x02;//segundo ou 9e (tentar depois)
  TX_DATA[2] = 0x02;//minuto
  TX_DATA[3] = 0x02;//hora
  TX_DATA[4] = 1;//dia da semana
  TX_DATA[5] = 0x02;//dia 10
  TX_DATA[6] = 0x02;//mes 12
  TX_DATA[7] = 0x02;//ano 15
  TX_DATA[8] = 0;
  */

  TX_DATA[0] = 0;
  TX_DATA[1] = 0;
  TX_DATA[2] = (RX_UART_string[16]-48)/10 + (RX_UART_string[17]-48);//segundo ou 9e (tentar depois)
  TX_DATA[3] = (RX_UART_string[13]-48)*10 + (RX_UART_string[14]-48);//minuto
  TX_DATA[4] = (RX_UART_string[10]-48)*10 + (RX_UART_string[11]-48);//hora
  TX_DATA[5] = 1;//dia da semana
  TX_DATA[6] = (RX_UART_string[1]-48)*10 + (RX_UART_string[2]-48);//dia 10
  TX_DATA[7] = (RX_UART_string[4]-48)*10 + (RX_UART_string[5]-48);//mes 12
  TX_DATA[8] = (RX_UART_string[7]-48)*10 + (RX_UART_string[8]-48);//ano 15
  TX_DATA[9] = 0x00;

  /*
  BIT 8
  01 nao rolou
  10000000 = 0x80 - nao deu erro, mas o contador estava parado
  10010000 = 0x90 - nao deu erro, mas o contador estava parado
  00010000 = 0x10
  10000011 = 0x83 -
  10010011 = 0x93 -
  00000011 = 0x03 - contador parado
  00010011 = 0x13 - contador parado
  00000000 = 0x00 - contador parado

  */
}

/* VARIAVEL DE FLAGS VIA SOFTWARE DA USCI
 * TX_NACK -> 1 SE ACK do ESCRAVO NAO recebida
 */
////////////////////// FUNCAO MAIN  //////////////////////////////
void main(void) {

  config_ini();
  P1_P2_config();
  config_UART();
  config_TA0();

  do{
    switch(flag){
      case 3:
        enviarHora();

        UCB0_config(40);//clock da i2c fica em 4mhz/16 =
        I2C_Enviar(0xD0,12);
        while(UCB0CTL1 & UCTXSTP);
        //I2C_Enviar(0x68, 2);
        flag=0;
      break;

      case 1:
        UCB0_config(16);
        //TX_DATA[0] = 0x00; // Endereco Reg. Interno
        //TX_DATA[1] = 0x00; // Dado Escravo
          etapa=0;
        I2C_Enviar(0x27, 2);
        I2C_Receber(0x27, 4);
        while(UCB0CTL1 & UCTXSTP );

        TEMP_DATA[0] = RX_DATA[1];
        TEMP_DATA[1] = RX_DATA[2];
        TEMP_DATA[2] = RX_DATA[3];
        TEMP_DATA[3] = RX_DATA[4];

        TEMP_DATA[0] &= ~(BIT6 + BIT7);
        umidade = (TEMP_DATA[0]*256) + TEMP_DATA[1];
        TEMP_DATA[3] = TEMP_DATA[3] >> 2;
        temperatura = (TEMP_DATA[2]*64) + TEMP_DATA[3];

        temperatura = (temperatura*165/16382)-40;
        umidade = (umidade*100/16382);

        //define a temperatura no vetor da uart
        TX_UART_string[23] = (temperatura/10)+48;
        TX_UART_string[24] = (temperatura%10)+48;

        //define a umidade no vetor da uart
        TX_UART_string[19] = (umidade/100)+48;
        TX_UART_string[20] = ((umidade%100)/10)+48;
        TX_UART_string[21] = ((umidade%100)%10)+48;


        //parte do RTC
        //RTC
        //RTC AQUI
        //UCB0_config(40);
        etapa=1;
        UCB0_config(40);
        I2C_Enviar(0xD0, 2);
        I2C_Receber(0xD0, 8); //D0 ta contando de 10 em 10
        while(UCB0CTL1 & UCTXSTP );

        RX_DATA2[5] &=~(BIT6+BIT7);
        TX_UART_string[1] = (RX_DATA2[5]/16)+48;
        TX_UART_string[2] = (RX_DATA2[5]%16)+48;//dia 01-31

        RX_DATA2[6] &=~(BIT7+BIT6+BIT5);
        TX_UART_string[4] = (RX_DATA2[6]/16)+48;//mes 01-12
        TX_UART_string[5] = (RX_DATA2[6]%16)+48;

        TX_UART_string[7] = (RX_DATA2[7]/16)+48;//ano 0-99
        TX_UART_string[8] = (RX_DATA2[7]%16)+48;

        RX_DATA2[3] &=~(BIT7+BIT6+BIT5);
        TX_UART_string[10] = (RX_DATA2[3]/16)+48;//hora 1-12 OU 0-23
        TX_UART_string[11] = (RX_DATA2[3]%16)+48;

        TX_UART_string[13] = (RX_DATA2[2]/16)+48;//min 00-59
        TX_UART_string[14] = (RX_DATA2[2]%16)+48;

        RX_DATA2[0] &=~(BIT7);
        TX_UART_string[16] = (RX_DATA2[1]/16)+48;//seg 00-59
        TX_UART_string[17] = (RX_DATA2[1]%16)+48;
        flag = 2;
      break;

      case 2:
        IFG2 |= UCA0TXIFG;
        flag = 0;
      break;

      default:
      break;
    }
  }while(1);

}
//----------------------------------------------------------------

// RTI do modo MESTRE da UCA0/UCB0
#pragma  vector=USCIAB0TX_VECTOR
__interrupt void USCIAB0TX_RTI(void){

/* *****************************   MODO I2C -> MESTRE   **********************************
 *
 * Quando ocorre uma TRANSMISSAO ou RECEPCAO pela USCI no modo MESTRE E
 * a interrupcao de TRANSMISSAO esta habilitada (UCB0TXIE/UCA0TXIE -> 1).
 *
 * CONSIDERANDO: UCB0TXIE/UCA0TXIE -> 1
 *  - Se for uma TRANSMISSAO, a flag UCB0TXIFG/UCA0TXIFG eh setada e cai nesta RTI
 *  - Se for uma RECEPCAO, a flag UCB0RXIFG/UCA0RXIFG eh setada e cai nesta MESMA RTI
 *
 */

// UCB0 MODO MESTRE -> TRANSMISSAO
  if(IFG2 & UCB0TXIFG){
    // Entra aqui apos a TRANSMISSAO do ENDERECO do ESCRAVO
    // ou quando um BYTE eh TRANSMITIDO

    IFG2 &= ~UCB0TXIFG;
    if(N_TX_BYTES <= 0){
      UCB0CTL1 |= UCTXSTP;
      TX_count = 0;
    }else{
      N_TX_BYTES--;
      UCB0TXBUF = TX_DATA[TX_count];
      TX_count++;
    }
  }

// UCB0 MODO MESTRE -> RECEPCAO
  if(IFG2 & UCB0RXIFG){
  // Entra se um Byte for recebido pela USCI no modo MESTRE
    if(etapa == 0){
      RX_DATA[RX_count] = UCB0RXBUF;  // Armazena dado recebido no vetor
    }else{
      RX_DATA2[RX_count] = UCB0RXBUF;  // Armazena dado recebido no vetor
    }
    RX_count++;

    N_RX_BYTES--;

    if(N_RX_BYTES < 1){
      RX_count = 0;
      UCB0CTL1 |= UCTXSTP;  // Se resta 1 Byte a receber apenas
    }
  }

// UCB0 MODO MESTRE -> NACK recebido
  if(UCB0STAT & UCNACKIFG){ // Se o ESCRAVO nao enviar confirmacao
      UCB0CTL1 |= UCTXSTP;      // Gera condicao STOP
      USCI_SW_FLAGS |= TX_NACK; // Indica que houve um NACK
  }
/* ************************** UCA0 ->   MODO SPI / MODO UART  **********************/
  if(IFG2 & UCA0TXIFG){
    // Usado no caso da implementacao de SPI ou UART
    // Entra se a interrupcao for gerada pela UCA0
    if(TX_UART_string[tx_UART_index] == '@'){
      tx_UART_index = 0;
      IFG2 &= ~UCA0TXIFG;  // Limpa a flag de int. para nao entrar mais na RTI
    }else{
      UCA0TXBUF = TX_UART_string[tx_UART_index];
      tx_UART_index++;
    }

  }
/* *********************************************************************************/
}

// RTI de RX da UCA0/UCB0
#pragma  vector=USCIAB0RX_VECTOR
__interrupt void USCIAB0RX_RTI(void){

/* ************************** UCB0 ->   MODO I2C   **********************************
 *
 * SOH ENTRA NESTA RTI NO MODO ESCRAVO DA I2C
 *
 * **************************************************************************************/
/* ************************** UCA0 ->   MODO MESTRE SPI  / MODO UART**********************/
  if(IFG2 & UCA0RXIFG){
    // Usado no caso da implementacao de SPI ou UART
    /* Se entrar aqui, como a UCA0 nao esta configurada,
     * entao le o valor de UCB0RXBUF e descarta
     */



    RX_UART_string[rx_UART_index] = UCA0RXBUF;
    rx_UART_index++;
    if(rx_UART_index >= 20) rx_UART_index = 0;

    if(rx_UART_index > 18){
      //habilita o TA0 - ele que inicia a UART que vai pro pc
      TA0CTL |= MC0;
      flag = 3;
    }
  }
}

void config_UART(void){

    UCA0CTL1 = UCSSEL1 + UCSWRST;  // Fonte clock: SMCLK ~ 4 MHz | Interface em estado de reset

    UCA0BR0 = 0xA0;  // Para 9600 bps, conforme Tabela 15-4 do User Guide
    UCA0BR1 = 0x01;

    UCA0MCTL = UCBRS1 + UCBRS2;  // UCBRF = 0; UCBRS = 6

    UCA0CTL1 &= ~UCSWRST; // Coloca a interface no modo normal

    IFG2 &= ~UCA0TXIFG;  // Limpa flag de int. de TX, pois o bit eh setado apos reset

    IE2 |= UCA0TXIE + UCA0RXIE; // Habilitando a geracao de int. para RX e TX
}

//////////////////////////////////////////////////////////////////////////////////
// Configuracoes iniciais: WDT e BCS
void config_ini(void){

  WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer

    // Configuracoes do BCS
    // LFXT1 desativado
    // MCLK = DCOCLK ~ 16 MHz
    // ACLK = desativado!!!
    // SMCLK = DCOCLK / 4 ~ 4 MHz

    DCOCTL = CALDCO_16MHZ;  // Usando valores de calibracao para 16MHz
    BCSCTL1 = CALBC1_16MHZ;
    BCSCTL2 = DIVS1;
    BCSCTL3 = XCAP0 + XCAP1;
    while(BCSCTL3 & LFXT1OF);
    __enable_interrupt();
}


void P1_P2_config(void){

  P1DIR = BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6 + BIT7;
  P1OUT = 0;
  P1SEL = BIT1 + BIT2;   // Conecta pinos 3 e 4 nos sinais UCA0TXD/UCA0RXD
  P1SEL2 = BIT1 + BIT2;

  P2DIR = BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6 + BIT7;
  P2OUT = 0;
}



void UCB0_config(int divisor){

/* INICIALIZACAO USCI
 *   1 - Setar bit UCSWRST (mantem USCI no estado de RESET)
 *   2 - Configurar os registrados da USCI com UCSWRST = 1
 *   3 - Configurar Porta 1 (alterar funcao dos pinos)
 *   4 - Limpar bit UCSWRST (coloca USCI em operacao)
 *   5 - Habilitar interrupcoes via UCxRXIE e/ou UCxTXIE
 */

  // Passo 1: Setar bit UCSWRST (mantem USCI no estado de RESET)
  UCB0CTL1 |= UCSWRST;

  // Passo 2: Configurar os registrados da USCI

  /* Configuracao de UCB0CTL0
   *  - Modo de endereco do MESTRE: 7 bits  (UCA10 -> 0)
   *  - Modo de endereco do ESCRAVO: 7 bits   (UCSLA10 -> 0)
   *  - Modo Multimestre: desabilitado        (UCMM -> 0)
   *  - Modo MESTRE/ESCRAVO: modo mestre    (UCMST -> 1)
   *  - Modo de operacao da USCI: I2C     (UCMODE0 -> 1; UCMODE1 -> 1)
   *  - Modo assincrono/sincrono: sincrono    (UCSYNC -> 1)
   */
  // UCB0CTL0    bit 7   bit 6    bit 5   bit 4    bit 3     bit 2    bit 1   bit 0
  //         UCA10  UCSLA10   UCMM   Resevado  UCMST   UCMODE0   UCMODE1  UCSYNC
  //         0    0    0      0      1    1     1     1
  UCB0CTL0 = UCMST + UCMODE0 + UCMODE1 + UCSYNC;

  /* Configuracao de UCB0CTL1
   *   - Fonte de clock: SMCLK         (UCSSEL0 -> 1; UCSSEL1 -> 1)
   *   - Transmissao/Recepcao: transmissao (UCTR -> 1)
   *   - Transmitir um NACK:  nao      (UCTXNACK -> 0)
   *   - Transmitir um STOP: nao       (UCTXSTP -> 0)
   *   - Transmitir um START: nao      (UCTXSTT -> 0)
   *   - Reset da USCI: manter       (UCSWRST -> 1)
   */
  // UCB0CTL1    bit 7    bit 6    bit 5    bit 4   bit 3     bit 2    bit 1    bit 0
  //        UCSSEL1  UCSSEL0  Resevado  UCTR   UCTXNACK  UCTXSTP  UCTXSTT  UCSWRST
  //         1     1       0    1   0     0     0     1
  UCB0CTL1 = UCSSEL1 + UCSSEL0 + UCTR + UCSWRST;

  /* UCB0BR0 e UCB0BR1
   *
   * Como SMCLK ~ 2MHz e o clock desejado para a I2C eh de 100 kHz, entao o fator
   * de divisao sera de 20
   *
   */
  UCB0BR0 = divisor;//$divisor
  UCB0BR1 = 0;

  // Passo 3: Configuracao da Porta 1
  // P1.6 como SCL e P1.7 como SDA
  P1SEL |= BIT6 + BIT7;
  P1SEL2 |= BIT6 + BIT7;

  // Passo 4: Limpar bit UCSWRST (coloca USCI em operacao)
  UCB0CTL1 &= ~UCSWRST;


  // Passo 5: Habilitar interrupcao de TX e RX
  IE2 |= UCB0TXIE + UCB0RXIE;
  /* Habilita interrucap quando um ACK NAO eh recebido do ESCRAVO
   *
   * Habilita interrucap do STOP. Quando um STOP eh gerado,  gera interrupcao
   * e na RTI reseta bit TX_OK/RX_OK de USCI_SW_FLAGS, indicando que
   * a comunicacao terminou
   */
  UCB0I2CIE = UCNACKIE + UCSTPIE;
}


void I2C_Enviar(volatile unsigned char SLAVE_ADDR, volatile unsigned char Num_bytes){
/*FUNCAO PARA ENVIO DE BYTES VIA I2C
 *
 * SLAVE_ADDR => Endereco do escravo
 * Comando  =>  codigo do byte de comando
 * Num_bytes => Numero de Bytes a ser transmitido
 * *P_dados  => Ponteiro para o vetor de dados
 *
 * OBS.: Antes de transmitir, os dados a serem transmitivos devem ser carregados em TX_DATA[]
 */

  /* UCB0STAT ->  UCBBUSY
   *
   * UCBBUSY eh setado apos um START e LIMPO apos um STOP
   * Entao pode ser usado para verificar se a USCI esta livre.
   *
   */

  while( UCB0STAT & UCBBUSY );

  N_TX_BYTES = Num_bytes;

  UCB0CTL1 |= UCTR;  // Seleciona transmissao
  UCB0I2CSA = SLAVE_ADDR;
  UCB0CTL1 |= UCTXSTT; // Start de transmissao

}

void I2C_Receber(volatile unsigned char SLAVE_ADDR, volatile unsigned char Num_bytes){
/*FUNCAO PARA RECEBER BYTES VIA I2C
 *
 * SLAVE_ADDR => Endereco do escravo
 * Num_bytes => Numero de Bytes a ser recebido
 * Os dados recebidos sao armazenados em RX_DATA
 */

  while( UCB0STAT & UCBBUSY ); // verificar se a USCI esta livre
  N_RX_BYTES = Num_bytes;
  UCB0CTL1 &= ~UCTR;  // Seleciona recepcao
  UCB0I2CSA = SLAVE_ADDR;
  UCB0CTL1 |= UCTXSTT; // Start de recepcao

}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void TACCR0_RTI(void){

    //desabilita interrupção da recepcao da uart, pois não é mais necessária
    IE2 &=~(UCA0RXIE);

    //led vermelho para testes e debug do funcionamento, não necessário
    P1OUT ^= BIT0;

    //inicia transmissao para o computador
    flag = 1;

}
