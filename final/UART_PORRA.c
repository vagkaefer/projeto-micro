/*************************************************************************
 * TITULO: Exemplo 1 UART
 * Autor: turma 6CP
 * Data: 10/09/2013
 *
 *     uC-----------
 *     				|
 *     				|-> P1.3 -------------------
 *     				|							|
 *     				|-> P1.1 - UCA0RXD	 		\ S2
 *     				|-> P1.2 - UCA0TXD			|
 *     				|							_
 *     				|
 *
 *  OBJETIVO: Enviar uma mensagem de texto atraves da UART quando a tecla
 *            S2 for pressionada.
 *            Receber caracteres enviados pelo computador atraves de vetor.
 *
 *            Configuracoes da UART: 9600 bps / 8 bits / sem paridade
 *
 *
 ***************************************************************************/


#include <msp430.h> 

void config_ini(void);
void ini_P1(void);
void config_UART(void);

const unsigned char TX_string[] = "UART Transmitindo!!! @";
unsigned char RX_string[32];

unsigned char tx_index = 0;
unsigned char rx_index = 0;


void main(void) {

	config_ini();
	ini_P1();
	config_UART();


	do{
		// Codigo
	}while(1);

}



//RTI de Transmissao Serial
#pragma vector=USCIAB0TX_VECTOR
__interrupt void RTI_TXD(void){

	if(TX_string[tx_index] == '@'){
		tx_index = 0;
		IFG2 &= ~UCA0TXIFG;  // Limpa a flag de int. para nao entrar mais na RTI
	}else{
		UCA0TXBUF = TX_string[tx_index];
		tx_index++;
	}

}


//RTI de Recepcao Serial
#pragma vector=USCIAB0RX_VECTOR
__interrupt void RTI_RXD(void){

   RX_string[rx_index] = UCA0RXBUF;
   rx_index++;

   if(rx_index >= 32) rx_index = 0;

}


void config_UART(void){

	UCA0CTL1 = UCSSEL1 + UCSWRST;  // Fonte clock: SMCLK ~ 4 MHz | Interface em estado de reset

	UCA0BR0 = 0xA0;  // Para 9600 bps, conforme Tabela 15-4 do User Guide
	UCA0BR1 = 0x01;

	UCA0MCTL = UCBRS1 + UCBRS2;  // UCBRF = 0; UCBRS = 6

	UCA0CTL1 &= ~UCSWRST; // Coloca a interface no modo normal

	IFG2 &= ~UCA0TXIFG;  // Limpa flag de int. de TX, pois o bit eh setado apos reset

	IE2 |= UCA0TXIE + UCA0RXIE; // Habilitando a geracao de int. para RX e TX

}




// RTI da P1
#pragma vector=PORT1_VECTOR
__interrupt void RTI_P1(void){

	P1IFG &= ~BIT3; // Limpa flag de int. de P1.3
	P1IE &= ~BIT3;  // Desabilita int. de P1.3

	WDTCTL = WDTPW + WDTTMSEL + WDTCNTCL + WDTIS0;
	// WDT no modo temporizador, SMCLK (3,75 MHz), fator 8192, clear no contador.
	// Tempo ~ 2 ms.

	IE1 = WDTIE; // Habilita int. WDT
}


// RTI do WDT
#pragma vector=WDT_VECTOR
__interrupt void RTI_WDT(void){

	WDTCTL = WDTPW + WDTHOLD;  // Para o contador do WDT

	if( (~P1IN) & BIT3){
		IFG2 |= UCA0TXIFG;  // Seta a flag de int. de TX para inicar a transmissao
	}

	P1IFG &= ~BIT3; // Limpa flag de int. de P1.3

	P1IE |= BIT3;  // Habilita int. de P1.3

}



void ini_P1(void){

	/*	P1.6 - Saida para LED VM
	 *  P1.3 - Entrada com resistor de Pull-up Habilitado
	 *
	 *  Demais bits da porta 1 nao utilizados config. como
	 *  saida em nivel logico baixo
	 *
	 *  Conecta pinos 3 e 4 nos sinais UCA0TXD/UCA0RXD
	 */

	P1DIR = BIT0 + BIT1 + BIT2 + BIT4 + BIT5 + BIT6 + BIT7; // P1DIR = ~BIT3;
	P1REN = BIT3; // Ativa resistor de P1.3
	P1OUT = BIT3; // Ativa funcao pull-up do resistor de P1.3

	P1SEL = BIT1 + BIT2;   // Conecta pinos 3 e 4 nos sinais UCA0TXD/UCA0RXD
	P1SEL2 = BIT1 + BIT2;

	P1IES = BIT3; // Interrupcao por borda de descida em P1.3

	P1IFG = 0; // Limpa Flag de int. da P1 para evitar uma falsa int.

	P1IE = BIT3; // Habilita int. de P1.3

}




void config_ini(void){

    WDTCTL = WDTPW + WDTHOLD;	// Para o WDT

    // Configuracoes do BCS
    // LFXT1 -> Cristal 32.768 Hz
    // DCO -> ~ 16 MHz
    // ACLK = LFXT1 = 32.768 Hz
    // MCLK = DCOCLK ~ 16MHz
    // SMCLK = DCOCLK / 4 ~ 4 MHz

	DCOCTL = CALDCO_16MHZ;   // DCO com freq. calibrada de 16 MHz

	BCSCTL1 = CALBC1_16MHZ;

    BCSCTL2 = DIVS1;
	
    BCSCTL3 = XCAP0 + XCAP1; // ou BCSCTL3 = 0x0D;

    while(BCSCTL3 & LFXT1OF);  // prossegue a execucao se o sinal de LFXT1 estiver estavel.

    __enable_interrupt(); // Habilita a geracao de interrupcao

}
