#include <msp430.h>
#include <string.h>

/*
 * P1.0 -> saida do clock externo para o RTC - 32768Hz
 * P1.6 -> SCL
 * P1.7 -> SDA
 *
 * Endereco sensor de temperatura para I2C -> 0x27 -> ultimo bit já esta setado para read = 1
 * Endereco RTC para I2C -> 0xD1 ultimo bit está como write = 0
 *
 * MCLK = 16MHz
 * SMCLK = 4MHz
 * ACLK = 32768Hz
 *
 *
 * main.c
 *
 * I2C config -> endereco de 7 bits + 0 para escrita ou 1 para leitura
 * Para receber alguma coisa, é preciso setar o N_RX_BYTES com a quantidade de bytes a ser recebida
 * 1 -> Colocar os dados no TX_DATA antes de enviar o I2C;
 */

#define RTC 0xD0
#define HIH 0x26

#define MAX_TX_I2C 10
#define MAX_RX_I2C 20

char flag_controle=0;

char TX_I2C_DATA[MAX_TX_I2C], RX_I2C_DATA[MAX_RX_I2C];
unsigned char N_RX_I2C_BYTES = 0, N_TX_I2C_BYTES = 0, RX_I2C_count = 0, TX_I2C_count = 0;

char TX_UART_string[30], RX_UART_string[19], HIH_vetor[4];
unsigned char tx_UART_index=0,rx_UART_index=0;

void config_I2C(void);
void config_clock(void);
void config_portas(void);
void Iniciar(void);
void EnviarHora(void);
void receber_I2C(unsigned char slave_addr, unsigned char Num_bytes);
void enviar_I2C(volatile unsigned char slave_addr, volatile unsigned char Num_bytes);

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    config_clock();
    config_portas();
    config_I2C();

    flag_controle = 10;

    strcpy(RX_UART_string, "<01,01,01;01,01,01>");
    Iniciar();


    do{

    } while (1);
}

void config_clock(void){
	WDTCTL = WDTPW + WDTHOLD;	//Para contador do WDT

	DCOCTL = CALDCO_16MHZ;		//DCO confi. para 16MHz
	BCSCTL1 = CALBC1_16MHZ;
	BCSCTL2 = DIVS1;			//Fator de divisao para SMCLK = 4
	BCSCTL3 = XCAP0 + XCAP1;	//Capacitancia de 12,5pF para o cristal

	while(BCSCTL3 & LFXT1OF);	//Aguarda osc. LFXT1 estabilizar

	__enable_interrupt();
}

void config_portas(void){
	P1DIR = 0xFF;
	P1OUT = BIT6 + BIT7;
	//P1REN = 0xFF;
	P1SEL = BIT1 + BIT2 + BIT6 + BIT7;		//SCL -> P1.6; SDA -> P1.7
	P1SEL2 = BIT1 + BIT2 + BIT6 + BIT7;

	P2DIR = 0xFF;
	P2OUT = 0;
}

void config_I2C(void){
	UCB0CTL1 |= UCSWRST;	//Mantem estado de reset

	UCB0CTL0 = UCMST + UCMODE0 + UCMODE1 + UCSYNC;	//Modo master; modo I2C -> 11; modo sincronizado

	UCB0CTL1 = UCSSEL1 + UCSSEL0 + UCSWRST;			//Seleciona SMCLK como fonte de clock; mantem estado de reset

	UCB0BR0 = 50;	//f = fsource/BaudRate -> =100kHz

	UCB0BR1 = 0;



	UCB0CTL1 &= ~UCSWRST;	//Tira do modo reset

	IFG2 &= ~UCB0TXIFG;				//Limpar flag de interrupcao de transmissao

	UCB0I2CIE = UCNACKIE;			//Habilita interrupcao quando escravo nao confirmar o ACK

	IE2 |= UCB0TXIE + UCB0RXIE;		//habilita interrupcao ao transmitir e receber dados
}

void EnviarHora(void){
	TX_I2C_DATA[0] = 0;
	TX_I2C_DATA[1] = (RX_UART_string[1]-48)*10 + (RX_UART_string[2]-48);    //Segundos
	TX_I2C_DATA[2] = (RX_UART_string[4]-48)*10 + (RX_UART_string[5]-48);    //Minutos
	TX_I2C_DATA[3] = (RX_UART_string[7]-48)*10 + (RX_UART_string[8]-48);    //Horas
	TX_I2C_DATA[4] = 1;                                                     //Dia -> Semana
	TX_I2C_DATA[5] = (RX_UART_string[10]-48)*10 + (RX_UART_string[11]-48);  //Dia -> Mes
	TX_I2C_DATA[6] = (RX_UART_string[13]-48)*10 + (RX_UART_string[14]-48);  //Mes
	TX_I2C_DATA[7] = (RX_UART_string[16]-48)*10 + (RX_UART_string[17]-48);  //Ano
	TX_I2C_DATA[8] = 0x00; //Configuracao
	enviar_I2C(RTC, 9);
}

void Iniciar(void){
	//enviar I2C
	//EnviarHora();

	//iniciar o timer
	TA0CTL = TASSEL0 + MC0;
	TA0CCTL0 = CCIE;

	TA0CCR0 = 32767;
	//receber_I2C(RTC, 7);
}

void receber_I2C(unsigned char slave_addr, unsigned char Num_bytes){
	while(UCB0STAT & UCBBUSY);

	N_RX_I2C_BYTES = Num_bytes;

	UCB0I2CSA = slave_addr+1;
	UCB0CTL1 &= ~UCTR;
	UCB0CTL1 |= UCTXSTT;

	while(UCB0CTL1 & UCTXSTT);
}

void enviar_I2C(volatile unsigned char slave_addr, volatile unsigned char Num_bytes){
	while(UCB0STAT & UCBBUSY);

	N_TX_I2C_BYTES = Num_bytes;	//Numero de bytes a serem enviados

	UCB0CTL1 |= UCTR;		//Seleciona a transmissao
	UCB0I2CSA = slave_addr;		//Seleciona o endereco do escravo
	UCB0CTL1 |= UCTXSTT;	//Inicia a transmissao

	while(UCB0CTL1 & UCTXSTT);
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void TACCR0_RTI(void){
	receber_I2C(RTC, 7);

	P1OUT ^= BIT0;
	//flag para calcular o ponto -> flag = 2
	//calcularPonto();

}

//RTI de Transmissao Serial
#pragma vector=USCIAB0TX_VECTOR         //Transmite UART e I2C e recebe I2C
__interrupt void RTI_TXD(void){
	if(IFG2 & UCB0TXIFG){	//entra na interrupcao apos uma transmissao de um byte
		IFG2 &= ~UCB0TXIFG;		//limpa flag de transmissao
		if(N_TX_I2C_BYTES < 1){	//se houver byte a ser transmitido ainda
			UCB0CTL1 |= UCTXSTP;	//para com a transmissao
			TX_I2C_count = 0;
		}else{
			N_TX_I2C_BYTES--;
			UCB0TXBUF = TX_I2C_DATA[TX_I2C_count];
			TX_I2C_count++;
		}
	}

	if(IFG2 & UCB0RXIFG){	//interrupcao para receber
		RX_I2C_DATA[RX_I2C_count] = UCB0RXBUF;	//Armazena o byte recebido
		RX_I2C_count++;						//incrementa vetor de recepcao
		N_RX_I2C_BYTES--;
		if(N_RX_I2C_BYTES < 1){
			UCB0CTL1 |= UCTXSTP;		//gera condicao de stop
			RX_I2C_count = 0;
		}
	}

	if(UCB0STAT & UCNACKIFG){	//se o escravo nao enviar confirmacao
		UCB0CTL1 |= UCTXSTP;	//gera condicao de stop
	}
}