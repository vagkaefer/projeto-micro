#include <msp430.h>
#include <string.h>

/*
 * P1.0 -> saida do clock externo para o RTC - 32768Hz
 * P1.6 -> SCL
 * P1.7 -> SDA
 *
 * Endereco sensor de temperatura para I2C -> 0x27 -> ultimo bit já esta setado para read = 1
 * Endereco RTC para I2C -> 0xD1 ultimo bit está como write = 0
 *
 * MCLK = 16MHz
 * SMCLK = 4MHz
 * ACLK = 32768Hz
 *
 *
 * I2C config -> endereco de 7 bits + 0 para escrita ou 1 para leitura
 * Para receber alguma coisa, é preciso setar o N_RX_BYTES com a quantidade de bytes a ser recebida
 * 1 -> Colocar os dados no TX_DATA antes de enviar o I2C;

O programa vai funcionar da seguinte forma:

- Será enviado a data e hora atual pelo computador <10,11,15;11,37,00> através da UART
- O MSP irá gerar uma RTI com a recepção dos dados, a RTI será desabilitada e irá enviar os dados para o RTC
- O TA0 será ativado na RTI da UART e irá gerar uma RTI a cada 1 segundo
- Na RTI do TA0 será feita a leitura da temperatura e humidade do sensor HIH, a hora no sensor RTC.
- Os dados serão enviados para o computador através da UART

HUMIDADE TERÁ 3 DIGITOS 0-100

humidade em % = 100*(saida_14_bits/(2^14)-2)

TEMPERATURA TERÁ 2 DIGITOS

temperatura em celsius = (saida_14_bits/(2^14)-2)*165 - 40;

 */

#define RTC 0xD0
#define HIH 0x26

#define MAX_TX_I2C 10
#define MAX_RX_I2C 10

/* VARIAVEIS GLOBAIS */

unsigned char TX_I2C_DATA[MAX_TX_I2C], RX_I2C_DATA[MAX_RX_I2C];
unsigned char N_RX_I2C_BYTES = 0, N_TX_I2C_BYTES = 0, RX_I2C_count = 0, TX_I2C_count = 0;
unsigned char TX_UART_string[30] = "<00,00,00;00,00,00;000;00>\n@", RX_UART_string[19], HIH_vetor[4];
unsigned char tx_UART_index=0,rx_UART_index=0;
int dia,mes,ano,hora,minuto,segundo;
float temperatura, humidade;
/* FIM VARIAVEIS GLOBAIS */

/* FUNCOES */

//configura o clock e habilita as interrupções
void config_clock(void){

	DCOCTL = CALDCO_16MHZ;		//DCO confi. para 16MHz
	BCSCTL1 = CALBC1_16MHZ;
	BCSCTL2 = DIVS1;			//Fator de divisao para SMCLK = 16/4 = 4MHz
	BCSCTL3 = XCAP0 + XCAP1;	//Capacitancia de 12,5pF para o cristal
	while(BCSCTL3 & LFXT1OF);	//Aguarda osc. LFXT1 estabilizar
	__enable_interrupt();
}

void config_portas(void){

	P1DIR = 0xFF;
	P1OUT = BIT6 + BIT7;
	P1SEL = BIT1 + BIT2 + BIT6 + BIT7;		//SCL -> P1.6; SDA -> P1.7
	P1SEL2 = BIT1 + BIT2 + BIT6 + BIT7;
	P2DIR = 0xFF;
	P2OUT = 0x00;
}

void config_UART(void){

	UCA0CTL1 = UCSSEL1 + UCSWRST;  // Fonte clock: SMCLK ~ 4 MHz | Interface em estado de reset
	UCA0BR0 = 0xA0;  // Para 9600 bps, conforme Tabela 15-4 do User Guide
	UCA0BR1 = 0x01;
	UCA0MCTL = UCBRS1 + UCBRS2;  // UCBRF = 0; UCBRS = 6
	UCA0CTL1 &= ~UCSWRST; // Coloca a interface no modo normal
	IFG2 &= ~UCA0TXIFG;  // Limpa flag de int. de TX, pois o bit eh setado apos reset
	IE2 |= UCA0TXIE + UCA0RXIE; // Habilitando a geracao de int. para RX e TX
}

void config_I2C(void){

	UCB0CTL1 |= UCSWRST;	//Mantem estado de reset
	UCB0CTL0 = UCMST + UCMODE0 + UCMODE1 + UCSYNC;	//Modo master; modo I2C -> 11; modo sincronizado
	UCB0CTL1 = UCSSEL1 + UCSSEL0 + UCSWRST;			//Seleciona SMCLK como fonte de clock; mantem estado de reset
	UCB0BR0 = 50;	//f = fsource/BaudRate -> =100kHz
	UCB0BR1 = 0;
	UCB0CTL1 &= ~UCSWRST;	//Tira do modo reset
	IFG2 &= ~UCB0TXIFG;				//Limpar flag de interrupcao de transmissao
	UCB0I2CIE = UCNACKIE;			//Habilita interrupcao quando escravo nao confirmar o ACK
	IE2 |= UCB0TXIE + UCB0RXIE;		//habilita interrupcao ao transmitir e receber dados
}

//configura o TA0 parado
void config_TA0(void){

	TA0CTL = TASSEL0;
	TA0CCTL0 = CCIE;
	//TA0CCR0 = 32767;//1seg
	TA0CCR0 = 65500;//2seg
}

void receber_I2C(unsigned char slave_addr, unsigned char Num_bytes){

	while(UCB0STAT & UCBBUSY);
	N_RX_I2C_BYTES = Num_bytes;
	UCB0I2CSA = slave_addr+1;
	UCB0CTL1 &= ~UCTR;
	UCB0CTL1 |= UCTXSTT;
	while(UCB0CTL1 & UCTXSTT);
}

/* FIM FUNCOES */

/* TRATAMENTOS DAS RTIs */

//RTI de Recepcao Serial
#pragma vector=USCIAB0RX_VECTOR     //Recebe UART
__interrupt void RTI_RXD(void){

   //le os dados
   RX_UART_string[rx_UART_index] = UCA0RXBUF;
   rx_UART_index++;
   if(rx_UART_index >= 20) rx_UART_index = 0;

   //grava os dados recebidos nas variaveis globais
   dia = (RX_UART_string[1]-48)*10 + (RX_UART_string[2]-48);
   mes = (RX_UART_string[4]-48)*10 + (RX_UART_string[5]-48);
   ano = (RX_UART_string[7]-48)*10 + (RX_UART_string[8]-48);
   hora = (RX_UART_string[10]-48)*10 + (RX_UART_string[11]-48);
   minuto = (RX_UART_string[13]-48)*10 + (RX_UART_string[14]-48);
   segundo = (RX_UART_string[16]-48)*10 + (RX_UART_string[17]-48);

   //habilita o timer TA0
   TA0CTL |= MC0;
}

//RTI do TA0 - Aqui acontece toda a magia
#pragma vector=TIMER0_A0_VECTOR
__interrupt void TACCR0_RTI(void){

	//desabilita interrupção da recepcao da uart, pois não é mais necessária
	//esta sendo feita aqui pois se fizer na RTI_RXD da pau
    IE2 &=~(UCA0RXIE);

	//led vermelho para testes e debug do funcionamento, não necessário
	P1OUT ^= BIT0;

	//AQUI TEM QUE PEGAR A HUMIDADE, TEMPERATURA, HORA E ENVIAR PRO PC

	//pega humidade e temperatura
	receber_I2C(HIH,4);

	//filtra os dados de humidade e temperatura
	RX_I2C_DATA[0] &= ~(BIT6 + BIT7);
	humidade = (RX_I2C_DATA[0]*256) + RX_I2C_DATA[1];
	RX_I2C_DATA[3] = RX_I2C_DATA[3] >> 2;
	temperatura = (RX_I2C_DATA[2]*64) + RX_I2C_DATA[3];

	humidade = (humidade/16382)*100;
	temperatura = ((temperatura/16382)*165)-40;

	//inicia transmissao para o computador
	IFG2 |= UCA0TXIFG;
}

//RTI de Transmissao Serial
#pragma vector=USCIAB0TX_VECTOR
__interrupt void RTI_TXD(void){

	//TRANSMISSAO UART
	if(IFG2 & UCA0TXIFG){
		if(TX_UART_string[tx_UART_index] == '@'){
			tx_UART_index = 0;
			IFG2 &= ~UCA0TXIFG;  // Limpa a flag de int. para nao entrar mais na RTI
		}else{
			UCA0TXBUF = TX_UART_string[tx_UART_index];
			tx_UART_index++;
		}
	}

	//TRANSMISSAO I2C
	if(IFG2 & UCB0RXIFG){	//interrupcao para receber
		RX_I2C_DATA[RX_I2C_count] = UCB0RXBUF;	//Armazena o byte recebido
		RX_I2C_count++;						//incrementa vetor de recepcao
		N_RX_I2C_BYTES--;
		if(N_RX_I2C_BYTES < 1){
			UCB0CTL1 |= UCTXSTP;		//gera condicao de stop
			RX_I2C_count = 0;
		}
	}

	if(UCB0STAT & UCNACKIFG){	//se o escravo nao enviar confirmacao
		UCB0CTL1 |= UCTXSTP;	//gera condicao de stop
	}
}

/* FIM TRATAMENTO DAS RTIs */

int main(void) {

    WDTCTL = WDTPW + WDTHOLD; //para o watchdog timer
    config_clock();
    config_portas();
    config_UART();
    config_TA0();
    config_I2C();

    do{

    	//dia
		//TX_UART_string[1] = (char)((dia/10)+48);
		//TX_UART_string[2] = (char)((dia%10)+48);

		//mes
		//TX_UART_string[4] = (char)((mes/10)+48);
		//TX_UART_string[5] = (char)((mes%10)+48);

		//ano
		//TX_UART_string[7] = (char)((ano/10)+48);
		//TX_UART_string[8] = (char)((ano%10)+48);

		//hora
		//TX_UART_string[10] = (char)((hora/10)+48);
		//TX_UART_string[11] = (char)((hora%10)+48);

		//min
		//TX_UART_string[13] = (char)((minuto/10)+48);
		//TX_UART_string[14] = (char)((minuto%10)+48);

		//seg
		//TX_UART_string[16] = (char)((segundo/10)+48);
		//TX_UART_string[17] = (char)((segundo%10)+48);

    	//humidade
		//TX_UART_string[19] = (char)(((int)humidade/100)+48);
		//TX_UART_string[20] = (char)((((int)humidade%100)/10)+48);
		//TX_UART_string[21] = (char)((((int)humidade%100)%10)+48);

    	//temp
		//TX_UART_string[23] = (char)(((int)temperatura/10)+48);
		//TX_UART_string[24] = (char)(((int)temperatura%10)+48);



    }while(1);
}
